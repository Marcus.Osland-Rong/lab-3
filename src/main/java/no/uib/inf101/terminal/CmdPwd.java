package no.uib.inf101.terminal;

public class CmdPwd implements Command {
    private Context context;

    @Override
    public String run(String[] args){
        return this.context.getCwd().getAbsolutePath();
    }

    @Override
    public String getName(){
        return "pwd";
    }

    @Override
    public String getManual() {
        return "Usage: pwd\n" +
               "Writes out the current directory.";
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
    }
}
