package no.uib.inf101.terminal;

import java.util.Map;

public class CmdCd implements Command {
    private Context context;

    @Override
    public String run(String[] args){
        if (args.length == 0) {
            this.context.goToHome();
            return "";
          } else if (args.length > 1) {
            return "cd: too many arguments";
          }
          String path = args[0];
          if (this.context.goToPath(path)) {
            return "";
          } else {
            return "cd: no such file or directory: " + path;
        }
        
    }

    @Override
    public String getName(){
        return "cd";
    }

    @Override
    public String getManual() {
        return "Usage: cd [directory]\n" +
               "changes the current directory to [option] if it exits.";
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public void setCommandContext(Map<String, Command> commandContext) {

    }
}
