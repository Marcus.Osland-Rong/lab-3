package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    private Context context;

    @Override
    public String run(String[] args){
        StringBuilder result = new StringBuilder();
        for (String arg : args){
            result.append(arg).append(" ");
        }
        return result.toString();
    }

    @Override
    public String getName(){
        return "echo";
    }

    @Override
    public String getManual() {
        return "Usage: echo [option]\n" +
               "Writes back [option].";
    }
    
}
