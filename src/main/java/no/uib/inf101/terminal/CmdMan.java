package no.uib.inf101.terminal;

import java.util.Map;

public class CmdMan implements Command{
    private Map<String, Command> commandContext;


    @Override
    public String run(String[] args){
        if (args.length == 0){
            return "No command provided.";
        }
        String cmdName = args[0];
        Command command = commandContext.get(cmdName);
        if (command != null){
            return command.getManual();
        }
        else{
            return "Cant find manual for " + cmdName;
        }
        
    }

    @Override
    public String getName() {
        return "man";
    }

    @Override
    public String getManual() {
        return "Usage: man [command]\n" +
               "Display manual for [command] if it exits.";
    }

    @Override
    public void setCommandContext(Map<String, Command> commandContext) {
        this.commandContext = commandContext;
    }
}
